FROM openjdk:11-jre
COPY target/dummy-trade-filler-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8082
ENTRYPOINT ["java", "-jar", "/app.jar"]